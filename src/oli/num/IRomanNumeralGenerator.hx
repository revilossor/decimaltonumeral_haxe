package oli.num;

interface IRomanNumeralGenerator
{
	private var numeralArrays(default,never):Array<Array<Dynamic>>;
	public function generate(decimal:Int):String;
	private function getNumeralArray(decimal:Int):Array<Dynamic>;
}