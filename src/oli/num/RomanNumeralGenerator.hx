package oli.num;

class RomanNumeralGenerator implements IRomanNumeralGenerator
{
	private var numeralArrays(default, never):Array<Array<Dynamic>> = [
		[1000,"M"],[900,"CM"],[500,"D"],[400,"CD"],[100,"C"],[90,"XC"],[50,"L"],[40,"XL"],[10,"X"],[9,"IX"],[5,"V"],[4,"IV"],[1,"I"],[0,""]
	];
	public function new(){}
	public function generate(decimal:Int):String {
		return getNumeral(decimal);
	}
	private function getNumeral(decimal:Int, numeral:String = ""):String {
		if(decimal == 0){return numeral;}
		var arr:Array<Dynamic> = getNumeralArray(decimal);
		return getNumeral(Std.int(decimal-arr[0]), numeral+arr[1]);
	}
	private function getNumeralArray(decimal:Int):Array<Dynamic> {
		for(arr in numeralArrays){
			if(decimal >= arr[0]){ return arr; }
		}
		var map:Array<Dynamic> = [1000,"M"];
		return map;
	}
}