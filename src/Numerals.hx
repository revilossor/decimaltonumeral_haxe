import oli.num.RomanNumeralGenerator;
import Sys;

class Numerals {
	private static var generator:RomanNumeralGenerator;
	static public function main()
	{
		generator = new RomanNumeralGenerator();
		#if (cpp || cs || java || macro || neko || php || python) // targets with CLI
			for(arg in Sys.args()){
				printNumeral(Std.parseInt(arg));
			}
		#else
			var decimal = 3999;
			printNumeral(decimal);
		#end
	}
	private static inline function printNumeral(decimal:Int):Void {
		trace('\t' + decimal + '\t:\t' + getNumeral(decimal));
	}
	private static inline function getNumeral(decimal:Int):String {
		return generator.generate(decimal);
	}
}