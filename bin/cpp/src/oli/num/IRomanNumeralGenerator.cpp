#include <hxcpp.h>

#ifndef INCLUDED_oli_num_IRomanNumeralGenerator
#include <oli/num/IRomanNumeralGenerator.h>
#endif
namespace oli{
namespace num{

HX_DEFINE_DYNAMIC_FUNC1(IRomanNumeralGenerator_obj,generate,return )

HX_DEFINE_DYNAMIC_FUNC1(IRomanNumeralGenerator_obj,getNumeralArray,return )


static ::String sMemberFields[] = {
	HX_CSTRING("numeralArrays"),
	HX_CSTRING("generate"),
	HX_CSTRING("getNumeralArray"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(IRomanNumeralGenerator_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(IRomanNumeralGenerator_obj::__mClass,"__mClass");
};

#endif

Class IRomanNumeralGenerator_obj::__mClass;

void IRomanNumeralGenerator_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("oli.num.IRomanNumeralGenerator"), hx::TCanCast< IRomanNumeralGenerator_obj> ,0,sMemberFields,
	0, 0,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , 0
#endif
);
}

void IRomanNumeralGenerator_obj::__boot()
{
}

} // end namespace oli
} // end namespace num
