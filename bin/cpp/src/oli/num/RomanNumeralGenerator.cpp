#include <hxcpp.h>

#ifndef INCLUDED_Std
#include <Std.h>
#endif
#ifndef INCLUDED_oli_num_IRomanNumeralGenerator
#include <oli/num/IRomanNumeralGenerator.h>
#endif
#ifndef INCLUDED_oli_num_RomanNumeralGenerator
#include <oli/num/RomanNumeralGenerator.h>
#endif
namespace oli{
namespace num{

Void RomanNumeralGenerator_obj::__construct()
{
HX_STACK_FRAME("oli.num.RomanNumeralGenerator","new",0x693d041a,"oli.num.RomanNumeralGenerator.new","oli/num/RomanNumeralGenerator.hx",3,0x8de30718)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(5)
	this->numeralArrays = Dynamic( Array_obj<Dynamic>::__new().Add(Dynamic( Array_obj<Dynamic>::__new().Add((int)1000).Add(HX_CSTRING("M")))).Add(Dynamic( Array_obj<Dynamic>::__new().Add((int)900).Add(HX_CSTRING("CM")))).Add(Dynamic( Array_obj<Dynamic>::__new().Add((int)500).Add(HX_CSTRING("D")))).Add(Dynamic( Array_obj<Dynamic>::__new().Add((int)400).Add(HX_CSTRING("CD")))).Add(Dynamic( Array_obj<Dynamic>::__new().Add((int)100).Add(HX_CSTRING("C")))).Add(Dynamic( Array_obj<Dynamic>::__new().Add((int)90).Add(HX_CSTRING("XC")))).Add(Dynamic( Array_obj<Dynamic>::__new().Add((int)50).Add(HX_CSTRING("L")))).Add(Dynamic( Array_obj<Dynamic>::__new().Add((int)40).Add(HX_CSTRING("XL")))).Add(Dynamic( Array_obj<Dynamic>::__new().Add((int)10).Add(HX_CSTRING("X")))).Add(Dynamic( Array_obj<Dynamic>::__new().Add((int)9).Add(HX_CSTRING("IX")))).Add(Dynamic( Array_obj<Dynamic>::__new().Add((int)5).Add(HX_CSTRING("V")))).Add(Dynamic( Array_obj<Dynamic>::__new().Add((int)4).Add(HX_CSTRING("IV")))).Add(Dynamic( Array_obj<Dynamic>::__new().Add((int)1).Add(HX_CSTRING("I")))).Add(Dynamic( Array_obj<Dynamic>::__new().Add((int)0).Add(HX_CSTRING("")))));
}
;
	return null();
}

//RomanNumeralGenerator_obj::~RomanNumeralGenerator_obj() { }

Dynamic RomanNumeralGenerator_obj::__CreateEmpty() { return  new RomanNumeralGenerator_obj; }
hx::ObjectPtr< RomanNumeralGenerator_obj > RomanNumeralGenerator_obj::__new()
{  hx::ObjectPtr< RomanNumeralGenerator_obj > result = new RomanNumeralGenerator_obj();
	result->__construct();
	return result;}

Dynamic RomanNumeralGenerator_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< RomanNumeralGenerator_obj > result = new RomanNumeralGenerator_obj();
	result->__construct();
	return result;}

hx::Object *RomanNumeralGenerator_obj::__ToInterface(const hx::type_info &inType) {
	if (inType==typeid( ::oli::num::IRomanNumeralGenerator_obj)) return operator ::oli::num::IRomanNumeralGenerator_obj *();
	return super::__ToInterface(inType);
}

::String RomanNumeralGenerator_obj::generate( int decimal){
	HX_STACK_FRAME("oli.num.RomanNumeralGenerator","generate",0x15e1693b,"oli.num.RomanNumeralGenerator.generate","oli/num/RomanNumeralGenerator.hx",10,0x8de30718)
	HX_STACK_THIS(this)
	HX_STACK_ARG(decimal,"decimal")
	HX_STACK_LINE(10)
	return this->getNumeral(decimal,null());
}


HX_DEFINE_DYNAMIC_FUNC1(RomanNumeralGenerator_obj,generate,return )

::String RomanNumeralGenerator_obj::getNumeral( int decimal,::String __o_numeral){
::String numeral = __o_numeral.Default(HX_CSTRING(""));
	HX_STACK_FRAME("oli.num.RomanNumeralGenerator","getNumeral",0x0cbc796e,"oli.num.RomanNumeralGenerator.getNumeral","oli/num/RomanNumeralGenerator.hx",12,0x8de30718)
	HX_STACK_THIS(this)
	HX_STACK_ARG(decimal,"decimal")
	HX_STACK_ARG(numeral,"numeral")
{
		HX_STACK_LINE(13)
		if (((decimal == (int)0))){
			HX_STACK_LINE(13)
			return numeral;
		}
		HX_STACK_LINE(14)
		Dynamic arr = this->getNumeralArray(decimal);		HX_STACK_VAR(arr,"arr");
		HX_STACK_LINE(15)
		int _g = ::Std_obj::_int((decimal - arr->__GetItem((int)0)));		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(15)
		return this->getNumeral(_g,(numeral + arr->__GetItem((int)1)));
	}
}


HX_DEFINE_DYNAMIC_FUNC2(RomanNumeralGenerator_obj,getNumeral,return )

Dynamic RomanNumeralGenerator_obj::getNumeralArray( int decimal){
	HX_STACK_FRAME("oli.num.RomanNumeralGenerator","getNumeralArray",0x2c6b754b,"oli.num.RomanNumeralGenerator.getNumeralArray","oli/num/RomanNumeralGenerator.hx",17,0x8de30718)
	HX_STACK_THIS(this)
	HX_STACK_ARG(decimal,"decimal")
	HX_STACK_LINE(18)
	{
		HX_STACK_LINE(18)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(18)
		Dynamic _g1 = this->numeralArrays;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(18)
		while((true)){
			HX_STACK_LINE(18)
			if ((!(((_g < _g1->__Field(HX_CSTRING("length"),true)))))){
				HX_STACK_LINE(18)
				break;
			}
			HX_STACK_LINE(18)
			Dynamic arr = _g1->__GetItem(_g);		HX_STACK_VAR(arr,"arr");
			HX_STACK_LINE(18)
			++(_g);
			HX_STACK_LINE(19)
			if (((decimal >= arr->__GetItem((int)0)))){
				HX_STACK_LINE(19)
				return arr;
			}
		}
	}
	HX_STACK_LINE(21)
	Dynamic map = Dynamic( Array_obj<Dynamic>::__new().Add((int)1000).Add(HX_CSTRING("M")));		HX_STACK_VAR(map,"map");
	HX_STACK_LINE(22)
	return map;
}


HX_DEFINE_DYNAMIC_FUNC1(RomanNumeralGenerator_obj,getNumeralArray,return )


RomanNumeralGenerator_obj::RomanNumeralGenerator_obj()
{
}

void RomanNumeralGenerator_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(RomanNumeralGenerator);
	HX_MARK_MEMBER_NAME(numeralArrays,"numeralArrays");
	HX_MARK_END_CLASS();
}

void RomanNumeralGenerator_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(numeralArrays,"numeralArrays");
}

Dynamic RomanNumeralGenerator_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 8:
		if (HX_FIELD_EQ(inName,"generate") ) { return generate_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"getNumeral") ) { return getNumeral_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"numeralArrays") ) { return numeralArrays; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"getNumeralArray") ) { return getNumeralArray_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic RomanNumeralGenerator_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 13:
		if (HX_FIELD_EQ(inName,"numeralArrays") ) { numeralArrays=inValue.Cast< Dynamic >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void RomanNumeralGenerator_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("numeralArrays"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*Dynamic*/ ,(int)offsetof(RomanNumeralGenerator_obj,numeralArrays),HX_CSTRING("numeralArrays")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("numeralArrays"),
	HX_CSTRING("generate"),
	HX_CSTRING("getNumeral"),
	HX_CSTRING("getNumeralArray"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(RomanNumeralGenerator_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(RomanNumeralGenerator_obj::__mClass,"__mClass");
};

#endif

Class RomanNumeralGenerator_obj::__mClass;

void RomanNumeralGenerator_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("oli.num.RomanNumeralGenerator"), hx::TCanCast< RomanNumeralGenerator_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void RomanNumeralGenerator_obj::__boot()
{
}

} // end namespace oli
} // end namespace num
