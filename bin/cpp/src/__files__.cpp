#include <hxcpp.h>

namespace hx {
const char *__hxcpp_all_files[] = {
#ifdef HXCPP_DEBUGGER
"C:\\HaxeToolkit\\haxe\\std/cpp/_std/Std.hx",
"C:\\HaxeToolkit\\haxe\\std/cpp/_std/Sys.hx",
"C:\\HaxeToolkit\\haxe\\std/haxe/Log.hx",
"C:\\HaxeToolkit\\haxe\\std/haxe/io/Eof.hx",
"Numerals.hx",
"oli/num/RomanNumeralGenerator.hx",
#endif
 0 };

const char *__hxcpp_all_files_fullpath[] = {
#ifdef HXCPP_DEBUGGER
"C:\\HaxeToolkit\\haxe\\std\\cpp\\_std\\Std.hx",
"C:\\HaxeToolkit\\haxe\\std\\cpp\\_std\\Sys.hx",
"C:\\HaxeToolkit\\haxe\\std\\haxe\\Log.hx",
"C:\\HaxeToolkit\\haxe\\std\\haxe\\io\\Eof.hx",
"c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\Numerals.hx",
"c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx",
#endif
 0 };

const char *__hxcpp_all_classes[] = {
#ifdef HXCPP_DEBUGGER
"Numerals",
"Std",
"Sys",
"haxe.Log",
"haxe.io.Eof",
"oli.num.RomanNumeralGenerator",
#endif
 0 };
} // namespace hx
void __files__boot() { __hxcpp_set_debugger_info(hx::__hxcpp_all_classes, hx::__hxcpp_all_files_fullpath); }
