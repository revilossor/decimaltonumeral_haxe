#include <hxcpp.h>

#include <oli/num/RomanNumeralGenerator.h>
#include <oli/num/IRomanNumeralGenerator.h>
#include <haxe/io/Eof.h>
#include <haxe/Log.h>
#include <Sys.h>
#include <Std.h>
#include <Numerals.h>

void __files__boot();

void __boot_all()
{
__files__boot();
hx::RegisterResources( hx::GetResources() );
::oli::num::RomanNumeralGenerator_obj::__register();
::oli::num::IRomanNumeralGenerator_obj::__register();
::haxe::io::Eof_obj::__register();
::haxe::Log_obj::__register();
::Sys_obj::__register();
::Std_obj::__register();
::Numerals_obj::__register();
::haxe::Log_obj::__boot();
::Numerals_obj::__boot();
::Std_obj::__boot();
::Sys_obj::__boot();
::haxe::io::Eof_obj::__boot();
::oli::num::IRomanNumeralGenerator_obj::__boot();
::oli::num::RomanNumeralGenerator_obj::__boot();
}

