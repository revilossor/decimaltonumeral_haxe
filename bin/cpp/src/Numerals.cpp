#include <hxcpp.h>

#ifndef INCLUDED_Numerals
#include <Numerals.h>
#endif
#ifndef INCLUDED_Std
#include <Std.h>
#endif
#ifndef INCLUDED_Sys
#include <Sys.h>
#endif
#ifndef INCLUDED_haxe_Log
#include <haxe/Log.h>
#endif
#ifndef INCLUDED_oli_num_IRomanNumeralGenerator
#include <oli/num/IRomanNumeralGenerator.h>
#endif
#ifndef INCLUDED_oli_num_RomanNumeralGenerator
#include <oli/num/RomanNumeralGenerator.h>
#endif

Void Numerals_obj::__construct()
{
	return null();
}

//Numerals_obj::~Numerals_obj() { }

Dynamic Numerals_obj::__CreateEmpty() { return  new Numerals_obj; }
hx::ObjectPtr< Numerals_obj > Numerals_obj::__new()
{  hx::ObjectPtr< Numerals_obj > result = new Numerals_obj();
	result->__construct();
	return result;}

Dynamic Numerals_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Numerals_obj > result = new Numerals_obj();
	result->__construct();
	return result;}

::oli::num::RomanNumeralGenerator Numerals_obj::generator;

Void Numerals_obj::main( ){
{
		HX_STACK_FRAME("Numerals","main",0xefb3cc52,"Numerals.main","Numerals.hx",7,0x501886e9)
		HX_STACK_LINE(8)
		::oli::num::RomanNumeralGenerator _g = ::oli::num::RomanNumeralGenerator_obj::__new();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(8)
		::Numerals_obj::generator = _g;
		HX_STACK_LINE(10)
		{
			HX_STACK_LINE(10)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(10)
			Array< ::String > _g11 = ::Sys_obj::args();		HX_STACK_VAR(_g11,"_g11");
			HX_STACK_LINE(10)
			while((true)){
				HX_STACK_LINE(10)
				if ((!(((_g1 < _g11->length))))){
					HX_STACK_LINE(10)
					break;
				}
				HX_STACK_LINE(10)
				::String arg = _g11->__get(_g1);		HX_STACK_VAR(arg,"arg");
				HX_STACK_LINE(10)
				++(_g1);
				HX_STACK_LINE(11)
				{
					HX_STACK_LINE(11)
					int decimal = ::Std_obj::parseInt(arg);		HX_STACK_VAR(decimal,"decimal");
					HX_STACK_LINE(11)
					::String _g12 = ::Numerals_obj::generator->generate(decimal);		HX_STACK_VAR(_g12,"_g12");
					HX_STACK_LINE(11)
					::String _g2 = (((HX_CSTRING("\t") + decimal) + HX_CSTRING("\t:\t")) + _g12);		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(11)
					::haxe::Log_obj::trace(_g2,hx::SourceInfo(HX_CSTRING("Numerals.hx"),19,HX_CSTRING("Numerals"),HX_CSTRING("printNumeral")));
				}
			}
		}
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC0(Numerals_obj,main,(void))

Void Numerals_obj::printNumeral( int decimal){
{
		HX_STACK_FRAME("Numerals","printNumeral",0x03a4904a,"Numerals.printNumeral","Numerals.hx",18,0x501886e9)
		HX_STACK_ARG(decimal,"decimal")
		HX_STACK_LINE(19)
		::String _g = ::Numerals_obj::generator->generate(decimal);		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(19)
		::String _g1 = (((HX_CSTRING("\t") + decimal) + HX_CSTRING("\t:\t")) + _g);		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(19)
		::haxe::Log_obj::trace(_g1,hx::SourceInfo(HX_CSTRING("Numerals.hx"),19,HX_CSTRING("Numerals"),HX_CSTRING("printNumeral")));
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(Numerals_obj,printNumeral,(void))

::String Numerals_obj::getNumeral( int decimal){
	HX_STACK_FRAME("Numerals","getNumeral",0x7e7f8ec1,"Numerals.getNumeral","Numerals.hx",22,0x501886e9)
	HX_STACK_ARG(decimal,"decimal")
	HX_STACK_LINE(22)
	return ::Numerals_obj::generator->generate(decimal);
}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(Numerals_obj,getNumeral,return )


Numerals_obj::Numerals_obj()
{
}

Dynamic Numerals_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"main") ) { return main_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"generator") ) { return generator; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"getNumeral") ) { return getNumeral_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"printNumeral") ) { return printNumeral_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Numerals_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 9:
		if (HX_FIELD_EQ(inName,"generator") ) { generator=inValue.Cast< ::oli::num::RomanNumeralGenerator >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Numerals_obj::__GetFields(Array< ::String> &outFields)
{
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("generator"),
	HX_CSTRING("main"),
	HX_CSTRING("printNumeral"),
	HX_CSTRING("getNumeral"),
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo *sMemberStorageInfo = 0;
#endif

static ::String sMemberFields[] = {
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Numerals_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(Numerals_obj::generator,"generator");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Numerals_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(Numerals_obj::generator,"generator");
};

#endif

Class Numerals_obj::__mClass;

void Numerals_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("Numerals"), hx::TCanCast< Numerals_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void Numerals_obj::__boot()
{
}

