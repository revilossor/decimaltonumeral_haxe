#ifndef INCLUDED_oli_num_IRomanNumeralGenerator
#define INCLUDED_oli_num_IRomanNumeralGenerator

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS2(oli,num,IRomanNumeralGenerator)
namespace oli{
namespace num{


class HXCPP_CLASS_ATTRIBUTES  IRomanNumeralGenerator_obj : public hx::Interface{
	public:
		typedef hx::Interface super;
		typedef IRomanNumeralGenerator_obj OBJ_;
		HX_DO_INTERFACE_RTTI;
		static void __boot();
virtual ::String generate( int decimal)=0;
		Dynamic generate_dyn();
virtual Dynamic getNumeralArray( int decimal)=0;
		Dynamic getNumeralArray_dyn();
};

#define DELEGATE_oli_num_IRomanNumeralGenerator \
virtual ::String generate( int decimal) { return mDelegate->generate(decimal);}  \
virtual Dynamic generate_dyn() { return mDelegate->generate_dyn();}  \
virtual Dynamic getNumeralArray( int decimal) { return mDelegate->getNumeralArray(decimal);}  \
virtual Dynamic getNumeralArray_dyn() { return mDelegate->getNumeralArray_dyn();}  \


template<typename IMPL>
class IRomanNumeralGenerator_delegate_ : public IRomanNumeralGenerator_obj
{
	protected:
		IMPL *mDelegate;
	public:
		IRomanNumeralGenerator_delegate_(IMPL *inDelegate) : mDelegate(inDelegate) {}
		hx::Object *__GetRealObject() { return mDelegate; }
		void __Visit(HX_VISIT_PARAMS) { HX_VISIT_OBJECT(mDelegate); }
		DELEGATE_oli_num_IRomanNumeralGenerator
};

} // end namespace oli
} // end namespace num

#endif /* INCLUDED_oli_num_IRomanNumeralGenerator */ 
