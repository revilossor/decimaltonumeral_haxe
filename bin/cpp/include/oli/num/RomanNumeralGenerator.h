#ifndef INCLUDED_oli_num_RomanNumeralGenerator
#define INCLUDED_oli_num_RomanNumeralGenerator

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <oli/num/IRomanNumeralGenerator.h>
HX_DECLARE_CLASS2(oli,num,IRomanNumeralGenerator)
HX_DECLARE_CLASS2(oli,num,RomanNumeralGenerator)
namespace oli{
namespace num{


class HXCPP_CLASS_ATTRIBUTES  RomanNumeralGenerator_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef RomanNumeralGenerator_obj OBJ_;
		RomanNumeralGenerator_obj();
		Void __construct();

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< RomanNumeralGenerator_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~RomanNumeralGenerator_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		inline operator ::oli::num::IRomanNumeralGenerator_obj *()
			{ return new ::oli::num::IRomanNumeralGenerator_delegate_< RomanNumeralGenerator_obj >(this); }
		hx::Object *__ToInterface(const hx::type_info &inType);
		::String __ToString() const { return HX_CSTRING("RomanNumeralGenerator"); }

		Dynamic numeralArrays;
		virtual ::String generate( int decimal);
		Dynamic generate_dyn();

		virtual ::String getNumeral( int decimal,::String numeral);
		Dynamic getNumeral_dyn();

		virtual Dynamic getNumeralArray( int decimal);
		Dynamic getNumeralArray_dyn();

};

} // end namespace oli
} // end namespace num

#endif /* INCLUDED_oli_num_RomanNumeralGenerator */ 
