#ifndef INCLUDED_Numerals
#define INCLUDED_Numerals

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS0(Numerals)
HX_DECLARE_CLASS2(oli,num,IRomanNumeralGenerator)
HX_DECLARE_CLASS2(oli,num,RomanNumeralGenerator)


class HXCPP_CLASS_ATTRIBUTES  Numerals_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef Numerals_obj OBJ_;
		Numerals_obj();
		Void __construct();

	public:
		inline void *operator new( size_t inSize, bool inContainer=false)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< Numerals_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~Numerals_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		::String __ToString() const { return HX_CSTRING("Numerals"); }

		static ::oli::num::RomanNumeralGenerator generator;
		static Void main( );
		static Dynamic main_dyn();

		static Void printNumeral( int decimal);
		static Dynamic printNumeral_dyn();

		static ::String getNumeral( int decimal);
		static Dynamic getNumeral_dyn();

};


#endif /* INCLUDED_Numerals */ 
