package haxe.io {
	import haxe.io._Error;
	import flash.utils.Endian;
	import flash.utils.ByteArray;
	import flash.Boot;
	public class Bytes {
		public function Bytes(length : int = 0,b : flash.utils.ByteArray = null) : void { if( !flash.Boot.skip_constructor ) {
			this.length = length;
			this.b = b;
			b.endian = flash.utils.Endian.LITTLE_ENDIAN;
		}}
		
		public var length : int;
		public var b : flash.utils.ByteArray;
		public function get(pos : int) : int {
			return this.b[pos];
		}
		
		public function set(pos : int,v : int) : void {
			this.b[pos] = v;
		}
		
		public function blit(pos : int,src : haxe.io.Bytes,srcpos : int,len : int) : void {
			if(pos < 0 || srcpos < 0 || len < 0 || pos + len > this.length || srcpos + len > src.length) throw haxe.io._Error.OutsideBounds;
			this.b.position = pos;
			if(len > 0) this.b.writeBytes(src.b,srcpos,len);
		}
		
		public function fill(pos : int,len : int,value : int) : void {
			var v4 : int = value & 255;
			v4 |= v4 << 8;
			v4 |= v4 << 16;
			this.b.position = pos;
			{
				var _g1 : int = 0;
				var _g : int = len >> 2;
				while(_g1 < _g) {
					var i : int = _g1++;
					this.b.writeUnsignedInt(v4);
				}
			};
			pos += (len & -4);
			{
				var _g11 : int = 0;
				var _g2 : int = len & 3;
				while(_g11 < _g2) {
					var i1 : int = _g11++;
					this.set(pos++,value);
				}
			}
		}
		
		public function sub(pos : int,len : int) : haxe.io.Bytes {
			if(pos < 0 || len < 0 || pos + len > this.length) throw haxe.io._Error.OutsideBounds;
			this.b.position = pos;
			var b2 : flash.utils.ByteArray = new flash.utils.ByteArray();
			this.b.readBytes(b2,0,len);
			return new haxe.io.Bytes(len,b2);
		}
		
		public function compare(other : haxe.io.Bytes) : int {
			var len : int;
			if(this.length < other.length) len = this.length;
			else len = other.length;
			var b1 : flash.utils.ByteArray = this.b;
			var b2 : flash.utils.ByteArray = other.b;
			b1.position = 0;
			b2.position = 0;
			b1.endian = flash.utils.Endian.BIG_ENDIAN;
			b2.endian = flash.utils.Endian.BIG_ENDIAN;
			{
				var _g1 : int = 0;
				var _g : int = len >> 2;
				while(_g1 < _g) {
					var i : int = _g1++;
					if(b1.readUnsignedInt() != b2.readUnsignedInt()) {
						b1.position -= 4;
						b2.position -= 4;
						var d : uint = b1.readUnsignedInt() - b2.readUnsignedInt();
						b1.endian = flash.utils.Endian.LITTLE_ENDIAN;
						b2.endian = flash.utils.Endian.LITTLE_ENDIAN;
						return d;
					}
				}
			};
			{
				var _g11 : int = 0;
				var _g2 : int = len & 3;
				while(_g11 < _g2) {
					var i1 : int = _g11++;
					if(b1.readUnsignedByte() != b2.readUnsignedByte()) {
						b1.endian = flash.utils.Endian.LITTLE_ENDIAN;
						b2.endian = flash.utils.Endian.LITTLE_ENDIAN;
						return b1[b1.position - 1] - b2[b2.position - 1];
					}
				}
			};
			b1.endian = flash.utils.Endian.LITTLE_ENDIAN;
			b2.endian = flash.utils.Endian.LITTLE_ENDIAN;
			return this.length - other.length;
		}
		
		public function getDouble(pos : int) : Number {
			this.b.position = pos;
			return this.b.readDouble();
		}
		
		public function getFloat(pos : int) : Number {
			this.b.position = pos;
			return this.b.readFloat();
		}
		
		public function setDouble(pos : int,v : Number) : void {
			this.b.position = pos;
			this.b.writeDouble(v);
		}
		
		public function setFloat(pos : int,v : Number) : void {
			this.b.position = pos;
			this.b.writeFloat(v);
		}
		
		public function getString(pos : int,len : int) : String {
			if(pos < 0 || len < 0 || pos + len > this.length) throw haxe.io._Error.OutsideBounds;
			this.b.position = pos;
			return this.b.readUTFBytes(len);
		}
		
		public function readString(pos : int,len : int) : String {
			return this.getString(pos,len);
		}
		
		public function toString() : String {
			this.b.position = 0;
			return this.b.readUTFBytes(this.length);
		}
		
		public function toHex() : String {
			var s : StringBuf = new StringBuf();
			var chars : Array = [];
			var str : String = "0123456789abcdef";
			{
				var _g1 : int = 0;
				var _g : int = str.length;
				while(_g1 < _g) {
					var i : int = _g1++;
					chars.push(str["charCodeAtHX"](i));
				}
			};
			{
				var _g11 : int = 0;
				var _g2 : int = this.length;
				while(_g11 < _g2) {
					var i1 : int = _g11++;
					var c : int = this.get(i1);
					s.addChar(chars[c >> 4]);
					s.addChar(chars[c & 15]);
				}
			};
			return s.toString();
		}
		
		public function getData() : flash.utils.ByteArray {
			return this.b;
		}
		
		static public function alloc(length : int) : haxe.io.Bytes {
			var b : flash.utils.ByteArray = new flash.utils.ByteArray();
			b.length = length;
			return new haxe.io.Bytes(length,b);
		}
		
		static public function ofString(s : String) : haxe.io.Bytes {
			var b : flash.utils.ByteArray = new flash.utils.ByteArray();
			b.writeUTFBytes(s);
			return new haxe.io.Bytes(b.length,b);
		}
		
		static public function ofData(b : flash.utils.ByteArray) : haxe.io.Bytes {
			return new haxe.io.Bytes(b.length,b);
		}
		
		static public function fastGet(b : flash.utils.ByteArray,pos : int) : int {
			return b[pos];
		}
		
	}
}
