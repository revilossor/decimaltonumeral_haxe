package haxe.io {
	import haxe.io.Eof;
	import haxe.io._Error;
	import haxe.io.Bytes;
	import haxe.io.Input;
	import flash.utils.ByteArray;
	public class Output {
		public function get bigEndian() : Boolean { return $bigEndian; }
		public function set bigEndian( __v : Boolean ) : void { set_bigEndian(__v); }
		protected var $bigEndian : Boolean;
		public function writeByte(c : int) : void {
			throw "Not implemented";
		}
		
		public function writeBytes(s : haxe.io.Bytes,pos : int,len : int) : int {
			var k : int = len;
			var b : flash.utils.ByteArray = s.getData();
			if(pos < 0 || len < 0 || pos + len > s.length) throw haxe.io._Error.OutsideBounds;
			while(k > 0) {
				this.writeByte(b[pos]);
				pos++;
				k--;
			};
			return len;
		}
		
		public function flush() : void {
		}
		
		public function close() : void {
		}
		
		public function set_bigEndian(b : Boolean) : Boolean {
			this.$bigEndian = b;
			return b;
		}
		
		public function write(s : haxe.io.Bytes) : void {
			var l : int = s.length;
			var p : int = 0;
			while(l > 0) {
				var k : int = this.writeBytes(s,p,l);
				if(k == 0) throw haxe.io._Error.Blocked;
				p += k;
				l -= k;
			}
		}
		
		public function writeFullBytes(s : haxe.io.Bytes,pos : int,len : int) : void {
			while(len > 0) {
				var k : int = this.writeBytes(s,pos,len);
				pos += k;
				len -= k;
			}
		}
		
		public function writeFloat(x : Number) : void {
			if(x == 0.0) {
				this.writeByte(0);
				this.writeByte(0);
				this.writeByte(0);
				this.writeByte(0);
				return;
			};
			var exp : int = Math.floor(Math.log(Math.abs(x)) / haxe.io.Output.LN2);
			var sig : int = Math.floor(Math.abs(x) / Math.pow(2,exp) * 8388608) & 8388607;
			var b4 : int;
			b4 = (exp + 127 >> 1 | (((exp > 0)?((x < 0)?128:64):((x < 0)?128:0))));
			var b3 : int = (exp + 127 << 7 & 255) | (sig >> 16 & 127);
			var b2 : int = sig >> 8 & 255;
			var b1 : int = sig & 255;
			if(this.bigEndian) {
				this.writeByte(b4);
				this.writeByte(b3);
				this.writeByte(b2);
				this.writeByte(b1);
			}
			else {
				this.writeByte(b1);
				this.writeByte(b2);
				this.writeByte(b3);
				this.writeByte(b4);
			}
		}
		
		public function writeDouble(x : Number) : void {
			if(x == 0.0) {
				this.writeByte(0);
				this.writeByte(0);
				this.writeByte(0);
				this.writeByte(0);
				this.writeByte(0);
				this.writeByte(0);
				this.writeByte(0);
				this.writeByte(0);
				return;
			};
			var exp : int = Math.floor(Math.log(Math.abs(x)) / haxe.io.Output.LN2);
			var sig : int = Math.floor(Math.abs(x) / Math.pow(2,exp) * Math.pow(2,52));
			var sig_h : int = sig & int(34359738367);
			var sig_l : int = Math.floor(sig / Math.pow(2,32));
			var b8 : int;
			b8 = (exp + 1023 >> 4 | (((exp > 0)?((x < 0)?128:64):((x < 0)?128:0))));
			var b7 : int = (exp + 1023 << 4 & 255) | (sig_l >> 16 & 15);
			var b6 : int = sig_l >> 8 & 255;
			var b5 : int = sig_l & 255;
			var b4 : int = sig_h >> 24 & 255;
			var b3 : int = sig_h >> 16 & 255;
			var b2 : int = sig_h >> 8 & 255;
			var b1 : int = sig_h & 255;
			if(this.bigEndian) {
				this.writeByte(b8);
				this.writeByte(b7);
				this.writeByte(b6);
				this.writeByte(b5);
				this.writeByte(b4);
				this.writeByte(b3);
				this.writeByte(b2);
				this.writeByte(b1);
			}
			else {
				this.writeByte(b1);
				this.writeByte(b2);
				this.writeByte(b3);
				this.writeByte(b4);
				this.writeByte(b5);
				this.writeByte(b6);
				this.writeByte(b7);
				this.writeByte(b8);
			}
		}
		
		public function writeInt8(x : int) : void {
			if(x < -128 || x >= 128) throw haxe.io._Error.Overflow;
			this.writeByte(x & 255);
		}
		
		public function writeInt16(x : int) : void {
			if(x < -32768 || x >= 32768) throw haxe.io._Error.Overflow;
			this.writeUInt16(x & 65535);
		}
		
		public function writeUInt16(x : int) : void {
			if(x < 0 || x >= 65536) throw haxe.io._Error.Overflow;
			if(this.bigEndian) {
				this.writeByte(x >> 8);
				this.writeByte(x & 255);
			}
			else {
				this.writeByte(x & 255);
				this.writeByte(x >> 8);
			}
		}
		
		public function writeInt24(x : int) : void {
			if(x < -8388608 || x >= 8388608) throw haxe.io._Error.Overflow;
			this.writeUInt24(x & 16777215);
		}
		
		public function writeUInt24(x : int) : void {
			if(x < 0 || x >= 16777216) throw haxe.io._Error.Overflow;
			if(this.bigEndian) {
				this.writeByte(x >> 16);
				this.writeByte(x >> 8 & 255);
				this.writeByte(x & 255);
			}
			else {
				this.writeByte(x & 255);
				this.writeByte(x >> 8 & 255);
				this.writeByte(x >> 16);
			}
		}
		
		public function writeInt32(x : int) : void {
			if(this.bigEndian) {
				this.writeByte(x >>> 24);
				this.writeByte(x >> 16 & 255);
				this.writeByte(x >> 8 & 255);
				this.writeByte(x & 255);
			}
			else {
				this.writeByte(x & 255);
				this.writeByte(x >> 8 & 255);
				this.writeByte(x >> 16 & 255);
				this.writeByte(x >>> 24);
			}
		}
		
		public function prepare(nbytes : int) : void {
		}
		
		public function writeInput(i : haxe.io.Input,bufsize : * = null) : void {
			if(bufsize == null) bufsize = 4096;
			var buf : haxe.io.Bytes = haxe.io.Bytes.alloc(bufsize);
			try {
				while(true) {
					var len : int = i.readBytes(buf,0,bufsize);
					if(len == 0) throw haxe.io._Error.Blocked;
					var p : int = 0;
					while(len > 0) {
						var k : int = this.writeBytes(buf,p,len);
						if(k == 0) throw haxe.io._Error.Blocked;
						p += k;
						len -= k;
					}
				}
			}
			catch( e : haxe.io.Eof ){
			}
		}
		
		public function writeString(s : String) : void {
			var b : haxe.io.Bytes = haxe.io.Bytes.ofString(s);
			this.writeFullBytes(b,0,b.length);
		}
		
		static protected var LN2 : Number = Math.log(2);
	}
}
