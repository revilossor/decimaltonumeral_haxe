package haxe.io {
	import haxe.io._Error;
	import haxe.io.Bytes;
	import flash.utils.Endian;
	import flash.utils.ByteArray;
	import flash.Boot;
	public class BytesBuffer {
		public function BytesBuffer() : void { if( !flash.Boot.skip_constructor ) {
			this.b = new flash.utils.ByteArray();
			this.b.endian = flash.utils.Endian.LITTLE_ENDIAN;
		}}
		
		protected var b : flash.utils.ByteArray;
		public function get length() : int { return get_length(); }
		public function set length( __v : int ) : void { $length = __v; }
		protected var $length : int;
		public function get_length() : int {
			return this.b.length;
		}
		
		public function addByte(byte : int) : void {
			this.b.writeByte(byte);
		}
		
		public function add(src : haxe.io.Bytes) : void {
			this.b.writeBytes(src.getData());
		}
		
		public function addString(v : String) : void {
			this.b.writeUTFBytes(v);
		}
		
		public function addFloat(v : Number) : void {
			this.b.writeFloat(v);
		}
		
		public function addDouble(v : Number) : void {
			this.b.writeDouble(v);
		}
		
		public function addBytes(src : haxe.io.Bytes,pos : int,len : int) : void {
			if(pos < 0 || len < 0 || pos + len > src.length) throw haxe.io._Error.OutsideBounds;
			if(len > 0) this.b.writeBytes(src.getData(),pos,len);
		}
		
		public function getBytes() : haxe.io.Bytes {
			var bytes : haxe.io.Bytes = new haxe.io.Bytes(this.b.length,this.b);
			this.b.position = 0;
			this.b = null;
			return bytes;
		}
		
	}
}
