package haxe.io {
	import haxe.io.Eof;
	import haxe.io._Error;
	import haxe.io.BytesBuffer;
	import haxe.io.Bytes;
	import flash.utils.ByteArray;
	public class Input {
		public function get bigEndian() : Boolean { return $bigEndian; }
		public function set bigEndian( __v : Boolean ) : void { set_bigEndian(__v); }
		protected var $bigEndian : Boolean;
		public function readByte() : int {
			throw "Not implemented";
			return 0;
		}
		
		public function readBytes(s : haxe.io.Bytes,pos : int,len : int) : int {
			var k : int = len;
			var b : flash.utils.ByteArray = s.getData();
			if(pos < 0 || len < 0 || pos + len > s.length) throw haxe.io._Error.OutsideBounds;
			while(k > 0) {
				b[pos] = int(this.readByte());
				pos++;
				k--;
			};
			return len;
		}
		
		public function close() : void {
		}
		
		public function set_bigEndian(b : Boolean) : Boolean {
			this.$bigEndian = b;
			return b;
		}
		
		public function readAll(bufsize : * = null) : haxe.io.Bytes {
			if(bufsize == null) bufsize = 16384;
			var buf : haxe.io.Bytes = haxe.io.Bytes.alloc(bufsize);
			var total : haxe.io.BytesBuffer = new haxe.io.BytesBuffer();
			try {
				while(true) {
					var len : int = this.readBytes(buf,0,bufsize);
					if(len == 0) throw haxe.io._Error.Blocked;
					total.addBytes(buf,0,len);
				}
			}
			catch( e : haxe.io.Eof ){
			};
			return total.getBytes();
		}
		
		public function readFullBytes(s : haxe.io.Bytes,pos : int,len : int) : void {
			while(len > 0) {
				var k : int = this.readBytes(s,pos,len);
				pos += k;
				len -= k;
			}
		}
		
		public function read(nbytes : int) : haxe.io.Bytes {
			var s : haxe.io.Bytes = haxe.io.Bytes.alloc(nbytes);
			var p : int = 0;
			while(nbytes > 0) {
				var k : int = this.readBytes(s,p,nbytes);
				if(k == 0) throw haxe.io._Error.Blocked;
				p += k;
				nbytes -= k;
			};
			return s;
		}
		
		public function readUntil(end : int) : String {
			var buf : StringBuf = new StringBuf();
			var last : int;
			while((last = this.readByte()) != end) buf.addChar(last);
			return buf.toString();
		}
		
		public function readLine() : String {
			var buf : StringBuf = new StringBuf();
			var last : int;
			var s : String;
			try {
				while((last = this.readByte()) != 10) buf.addChar(last);
				s = buf.toString();
				if(s["charCodeAtHX"](s.length - 1) == 13) s = s.substr(0,-1);
			}
			catch( e : haxe.io.Eof ){
				s = buf.toString();
				if(s.length == 0) throw e;
			};
			return s;
		}
		
		public function readFloat() : Number {
			var bytes : Array = [];
			bytes.push(int(this.readByte()));
			bytes.push(int(this.readByte()));
			bytes.push(int(this.readByte()));
			bytes.push(int(this.readByte()));
			if(!this.bigEndian) bytes.reverse();
			var sign : int = 1 - (bytes[0] >> 7 << 1);
			var exp : int = ((bytes[0] << 1 & 255) | bytes[1] >> 7) - 127;
			var sig : int = ((bytes[1] & 127) << 16 | bytes[2] << 8) | bytes[3];
			if(sig == 0 && exp == -127) return 0.0;
			return sign * (1 + Math.pow(2,-23) * sig) * Math.pow(2,exp);
		}
		
		public function readDouble() : Number {
			var bytes : Array = [];
			bytes.push(this.readByte());
			bytes.push(this.readByte());
			bytes.push(this.readByte());
			bytes.push(this.readByte());
			bytes.push(this.readByte());
			bytes.push(this.readByte());
			bytes.push(this.readByte());
			bytes.push(this.readByte());
			if(!this.bigEndian) bytes.reverse();
			var sign : int = 1 - (bytes[0] >> 7 << 1);
			var exp : int = ((bytes[0] << 4 & 2047) | bytes[1] >> 4) - 1023;
			var sig : Number = this.getDoubleSig(bytes);
			if(sig == 0 && exp == -1023) return 0.0;
			return sign * (1.0 + Math.pow(2,-52) * sig) * Math.pow(2,exp);
		}
		
		public function readInt8() : int {
			var n : int = this.readByte();
			if(n >= 128) return n - 256;
			return n;
		}
		
		public function readInt16() : int {
			var ch1 : int = this.readByte();
			var ch2 : int = this.readByte();
			var n : int;
			if(this.bigEndian) n = (ch2 | ch1 << 8);
			else n = (ch1 | ch2 << 8);
			if((n & 32768) != 0) return n - 65536;
			return n;
		}
		
		public function readUInt16() : int {
			var ch1 : int = this.readByte();
			var ch2 : int = this.readByte();
			if(this.bigEndian) return ch2 | ch1 << 8;
			else return ch1 | ch2 << 8;
			return 0;
		}
		
		public function readInt24() : int {
			var ch1 : int = this.readByte();
			var ch2 : int = this.readByte();
			var ch3 : int = this.readByte();
			var n : int;
			if(this.bigEndian) n = ((ch3 | ch2 << 8) | ch1 << 16);
			else n = ((ch1 | ch2 << 8) | ch3 << 16);
			if((n & 8388608) != 0) return n - 16777216;
			return n;
		}
		
		public function readUInt24() : int {
			var ch1 : int = this.readByte();
			var ch2 : int = this.readByte();
			var ch3 : int = this.readByte();
			if(this.bigEndian) return (ch3 | ch2 << 8) | ch1 << 16;
			else return (ch1 | ch2 << 8) | ch3 << 16;
			return 0;
		}
		
		public function readInt32() : int {
			var ch1 : int = this.readByte();
			var ch2 : int = this.readByte();
			var ch3 : int = this.readByte();
			var ch4 : int = this.readByte();
			if(this.bigEndian) return ((ch4 | ch3 << 8) | ch2 << 16) | ch1 << 24;
			else return ((ch1 | ch2 << 8) | ch3 << 16) | ch4 << 24;
			return 0;
		}
		
		public function readString(len : int) : String {
			var b : haxe.io.Bytes = haxe.io.Bytes.alloc(len);
			this.readFullBytes(b,0,len);
			return b.toString();
		}
		
		protected function getDoubleSig(bytes : Array) : Number {
			return (((bytes[1] & 15) << 16 | bytes[2] << 8) | bytes[3]) * 4294967296. + (bytes[4] >> 7) * 2147483648 + ((((bytes[4] & 127) << 24 | bytes[5] << 16) | bytes[6] << 8) | bytes[7]);
		}
		
	}
}
