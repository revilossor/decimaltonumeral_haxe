package oli.num {
	import oli.num.IRomanNumeralGenerator;
	public class RomanNumeralGenerator implements oli.num.IRomanNumeralGenerator{
		public function RomanNumeralGenerator() : void {
		}
		
		protected var numeralArrays : Array = [[1000,"M"],[900,"CM"],[500,"D"],[400,"CD"],[100,"C"],[90,"XC"],[50,"L"],[40,"XL"],[10,"X"],[9,"IX"],[5,"V"],[4,"IV"],[1,"I"],[0,""]];
		public function generate(decimal : int) : String {
			return this.getNumeral(decimal);
		}
		
		protected function getNumeral(decimal : int,numeral : String = "") : String {
			if(numeral==null) numeral="";
			if(decimal == 0) return numeral;
			var arr : Array = this.getNumeralArray(decimal);
			return this.getNumeral(Std._int(decimal - arr[0]),numeral + arr[1]);
		}
		
		protected function getNumeralArray(decimal : int) : Array {
			{
				var _g : int = 0;
				var _g1 : Array = this.numeralArrays;
				while(_g < _g1.length) {
					var arr : Array = _g1[_g];
					++_g;
					if(decimal >= arr[0]) return arr;
				}
			};
			var map : Array = [1000,"M"];
			return map;
		}
		
	}
}
