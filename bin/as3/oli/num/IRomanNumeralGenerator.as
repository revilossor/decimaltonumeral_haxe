package oli.num {
	public interface IRomanNumeralGenerator {
		
		function generate(decimal : int) : String ;
		function getNumeralArray(decimal : int) : Array ;
	}
}
