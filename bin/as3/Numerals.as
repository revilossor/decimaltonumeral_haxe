package  {
	import haxe.Log;
	import oli.num.RomanNumeralGenerator;
	public class Numerals {
		static protected var generator : oli.num.RomanNumeralGenerator;
		static public function main() : void {
			Numerals.generator = new oli.num.RomanNumeralGenerator();
			var decimal : int = 3999;
			Numerals.printNumeral(3999);
		}
		
		static protected function printNumeral(decimal : int) : void {
			haxe.Log._trace("\t" + decimal + "\t:\t" + Numerals.getNumeral(decimal),{ fileName : "Numerals.hx", lineNumber : 19, className : "Numerals", methodName : "printNumeral"});
		}
		
		static protected function getNumeral(decimal : int) : String {
			return Numerals.generator.generate(decimal);
		}
		
	}
}
