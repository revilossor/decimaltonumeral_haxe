package oli.num;
import haxe.root.*;

@SuppressWarnings(value={"rawtypes", "unchecked"})
public  interface IRomanNumeralGenerator extends haxe.lang.IHxObject
{
	   java.lang.String generate(int decimal);
	
	   haxe.root.Array getNumeralArray(int decimal);
	
}


