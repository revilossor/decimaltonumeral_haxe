package oli.num;
import haxe.root.*;

@SuppressWarnings(value={"rawtypes", "unchecked"})
public  class RomanNumeralGenerator extends haxe.lang.HxObject implements oli.num.IRomanNumeralGenerator
{
	public    RomanNumeralGenerator(haxe.lang.EmptyObject empty)
	{
		{
		}
		
	}
	
	
	public    RomanNumeralGenerator()
	{
		oli.num.RomanNumeralGenerator.__hx_ctor_oli_num_RomanNumeralGenerator(this);
	}
	
	
	public static   void __hx_ctor_oli_num_RomanNumeralGenerator(oli.num.RomanNumeralGenerator __temp_me18)
	{
		{
			__temp_me18.numeralArrays = new haxe.root.Array<haxe.root.Array>(new haxe.root.Array[]{new haxe.root.Array(new java.lang.Object[]{1000, "M"}), new haxe.root.Array(new java.lang.Object[]{900, "CM"}), new haxe.root.Array(new java.lang.Object[]{500, "D"}), new haxe.root.Array(new java.lang.Object[]{400, "CD"}), new haxe.root.Array(new java.lang.Object[]{100, "C"}), new haxe.root.Array(new java.lang.Object[]{90, "XC"}), new haxe.root.Array(new java.lang.Object[]{50, "L"}), new haxe.root.Array(new java.lang.Object[]{40, "XL"}), new haxe.root.Array(new java.lang.Object[]{10, "X"}), new haxe.root.Array(new java.lang.Object[]{9, "IX"}), new haxe.root.Array(new java.lang.Object[]{5, "V"}), new haxe.root.Array(new java.lang.Object[]{4, "IV"}), new haxe.root.Array(new java.lang.Object[]{1, "I"}), new haxe.root.Array(new java.lang.Object[]{0, ""})});
		}
		
	}
	
	
	public static   java.lang.Object __hx_createEmpty()
	{
		return new oli.num.RomanNumeralGenerator(((haxe.lang.EmptyObject) (haxe.lang.EmptyObject.EMPTY) ));
	}
	
	
	public static   java.lang.Object __hx_create(haxe.root.Array arr)
	{
		return new oli.num.RomanNumeralGenerator();
	}
	
	
	public  haxe.root.Array<haxe.root.Array> numeralArrays;
	
	public   java.lang.String generate(int decimal)
	{
		return this.getNumeral(decimal, null);
	}
	
	
	public   java.lang.String getNumeral(int decimal, java.lang.String numeral)
	{
		if (( numeral == null )) 
		{
			numeral = "";
		}
		
		if (( decimal == 0 )) 
		{
			return numeral;
		}
		
		haxe.root.Array arr = this.getNumeralArray(decimal);
		return this.getNumeral(((int) (( ((double) (decimal) ) - ((double) (haxe.lang.Runtime.toDouble(arr.__get(0))) ) )) ), ( numeral + haxe.root.Std.string(arr.__get(1)) ));
	}
	
	
	public   haxe.root.Array getNumeralArray(int decimal)
	{
		{
			int _g = 0;
			haxe.root.Array<haxe.root.Array> _g1 = this.numeralArrays;
			while (( _g < _g1.length ))
			{
				haxe.root.Array arr = _g1.__get(_g);
				 ++ _g;
				if (( haxe.lang.Runtime.compare(decimal, arr.__get(0)) >= 0 )) 
				{
					return arr;
				}
				
			}
			
		}
		
		haxe.root.Array map = new haxe.root.Array(new java.lang.Object[]{1000, "M"});
		return map;
	}
	
	
	@Override public   java.lang.Object __hx_setField(java.lang.String field, java.lang.Object value, boolean handleProperties)
	{
		{
			boolean __temp_executeDef55 = true;
			switch (field.hashCode())
			{
				case -265662216:
				{
					if (field.equals("numeralArrays")) 
					{
						__temp_executeDef55 = false;
						this.numeralArrays = ((haxe.root.Array<haxe.root.Array>) (value) );
						return value;
					}
					
					break;
				}
				
				
			}
			
			if (__temp_executeDef55) 
			{
				return super.__hx_setField(field, value, handleProperties);
			}
			 else 
			{
				throw null;
			}
			
		}
		
	}
	
	
	@Override public   java.lang.Object __hx_getField(java.lang.String field, boolean throwErrors, boolean isCheck, boolean handleProperties)
	{
		{
			boolean __temp_executeDef56 = true;
			switch (field.hashCode())
			{
				case 1202298193:
				{
					if (field.equals("getNumeralArray")) 
					{
						__temp_executeDef56 = false;
						return ((haxe.lang.Function) (new haxe.lang.Closure(((java.lang.Object) (this) ), haxe.lang.Runtime.toString("getNumeralArray"))) );
					}
					
					break;
				}
				
				
				case -265662216:
				{
					if (field.equals("numeralArrays")) 
					{
						__temp_executeDef56 = false;
						return this.numeralArrays;
					}
					
					break;
				}
				
				
				case -963553816:
				{
					if (field.equals("getNumeral")) 
					{
						__temp_executeDef56 = false;
						return ((haxe.lang.Function) (new haxe.lang.Closure(((java.lang.Object) (this) ), haxe.lang.Runtime.toString("getNumeral"))) );
					}
					
					break;
				}
				
				
				case 1810371957:
				{
					if (field.equals("generate")) 
					{
						__temp_executeDef56 = false;
						return ((haxe.lang.Function) (new haxe.lang.Closure(((java.lang.Object) (this) ), haxe.lang.Runtime.toString("generate"))) );
					}
					
					break;
				}
				
				
			}
			
			if (__temp_executeDef56) 
			{
				return super.__hx_getField(field, throwErrors, isCheck, handleProperties);
			}
			 else 
			{
				throw null;
			}
			
		}
		
	}
	
	
	@Override public   java.lang.Object __hx_invokeField(java.lang.String field, haxe.root.Array dynargs)
	{
		{
			boolean __temp_executeDef57 = true;
			switch (field.hashCode())
			{
				case 1202298193:
				{
					if (field.equals("getNumeralArray")) 
					{
						__temp_executeDef57 = false;
						return this.getNumeralArray(((int) (haxe.lang.Runtime.toInt(dynargs.__get(0))) ));
					}
					
					break;
				}
				
				
				case 1810371957:
				{
					if (field.equals("generate")) 
					{
						__temp_executeDef57 = false;
						return this.generate(((int) (haxe.lang.Runtime.toInt(dynargs.__get(0))) ));
					}
					
					break;
				}
				
				
				case -963553816:
				{
					if (field.equals("getNumeral")) 
					{
						__temp_executeDef57 = false;
						return this.getNumeral(((int) (haxe.lang.Runtime.toInt(dynargs.__get(0))) ), haxe.lang.Runtime.toString(dynargs.__get(1)));
					}
					
					break;
				}
				
				
			}
			
			if (__temp_executeDef57) 
			{
				return super.__hx_invokeField(field, dynargs);
			}
			 else 
			{
				throw null;
			}
			
		}
		
	}
	
	
	@Override public   void __hx_getFields(haxe.root.Array<java.lang.String> baseArr)
	{
		baseArr.push("numeralArrays");
		{
			super.__hx_getFields(baseArr);
		}
		
	}
	
	
}


