package haxe.root;
import haxe.root.*;

@SuppressWarnings(value={"rawtypes", "unchecked"})
public  class Sys extends haxe.lang.HxObject
{
	public    Sys(haxe.lang.EmptyObject empty)
	{
		{
		}
		
	}
	
	
	public    Sys()
	{
		haxe.root.Sys.__hx_ctor__Sys(this);
	}
	
	
	public static   void __hx_ctor__Sys(haxe.root.Sys __temp_me6)
	{
		{
		}
		
	}
	
	
	public static  java.lang.String[] _args;
	
	public static   haxe.root.Array<java.lang.String> args()
	{
		if (( haxe.root.Sys._args == null )) 
		{
			return new haxe.root.Array<java.lang.String>(new java.lang.String[]{});
		}
		
		return haxe.java.Lib.array(haxe.root.Sys._args);
	}
	
	
	public static   java.lang.Object __hx_createEmpty()
	{
		return new haxe.root.Sys(((haxe.lang.EmptyObject) (haxe.lang.EmptyObject.EMPTY) ));
	}
	
	
	public static   java.lang.Object __hx_create(haxe.root.Array arr)
	{
		return new haxe.root.Sys();
	}
	
	
}


