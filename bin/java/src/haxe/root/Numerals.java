package haxe.root;
import haxe.root.*;

@SuppressWarnings(value={"rawtypes", "unchecked"})
public  class Numerals extends haxe.lang.HxObject
{
	public static void main(String[] args)
	{
		Sys._args = args;
		main();
	}
	public    Numerals(haxe.lang.EmptyObject empty)
	{
		{
		}
		
	}
	
	
	public    Numerals()
	{
		haxe.root.Numerals.__hx_ctor__Numerals(this);
	}
	
	
	public static   void __hx_ctor__Numerals(haxe.root.Numerals __temp_me3)
	{
		{
		}
		
	}
	
	
	public static  oli.num.RomanNumeralGenerator generator;
	
	public static   void main()
	{
		haxe.root.Numerals.generator = new oli.num.RomanNumeralGenerator();
		{
			int _g = 0;
			haxe.root.Array<java.lang.String> _g1 = haxe.root.Sys.args();
			while (( _g < _g1.length ))
			{
				java.lang.String arg = _g1.__get(_g);
				 ++ _g;
				{
					int decimal = ((int) (haxe.lang.Runtime.toInt(haxe.root.Std.parseInt(arg))) );
					haxe.Log.trace.__hx_invoke2_o(0.0, ( ( ( "\t" + decimal ) + "\t:\t" ) + haxe.root.Numerals.generator.generate(decimal) ), 0.0, new haxe.lang.DynamicObject(new haxe.root.Array<java.lang.String>(new java.lang.String[]{"className", "fileName", "methodName"}), new haxe.root.Array<java.lang.Object>(new java.lang.Object[]{"Numerals", "Numerals.hx", "printNumeral"}), new haxe.root.Array<java.lang.String>(new java.lang.String[]{"lineNumber"}), new haxe.root.Array<java.lang.Object>(new java.lang.Object[]{((java.lang.Object) (((double) (19) )) )})));
				}
				
			}
			
		}
		
	}
	
	
	public static   void printNumeral(int decimal)
	{
		haxe.Log.trace.__hx_invoke2_o(0.0, ( ( ( "\t" + decimal ) + "\t:\t" ) + haxe.root.Numerals.generator.generate(decimal) ), 0.0, new haxe.lang.DynamicObject(new haxe.root.Array<java.lang.String>(new java.lang.String[]{"className", "fileName", "methodName"}), new haxe.root.Array<java.lang.Object>(new java.lang.Object[]{"Numerals", "Numerals.hx", "printNumeral"}), new haxe.root.Array<java.lang.String>(new java.lang.String[]{"lineNumber"}), new haxe.root.Array<java.lang.Object>(new java.lang.Object[]{((java.lang.Object) (((double) (19) )) )})));
	}
	
	
	public static   java.lang.String getNumeral(int decimal)
	{
		return haxe.root.Numerals.generator.generate(decimal);
	}
	
	
	public static   java.lang.Object __hx_createEmpty()
	{
		return new haxe.root.Numerals(((haxe.lang.EmptyObject) (haxe.lang.EmptyObject.EMPTY) ));
	}
	
	
	public static   java.lang.Object __hx_create(haxe.root.Array arr)
	{
		return new haxe.root.Numerals();
	}
	
	
}


