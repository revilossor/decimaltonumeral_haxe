(function () { "use strict";
var Numerals = function() { };
Numerals.main = function() {
	Numerals.generator = new oli.num.RomanNumeralGenerator();
	var decimal = 3999;
	console.log("\t" + 3999 + "\t:\t" + Numerals.generator.generate(3999));
};
Numerals.printNumeral = function(decimal) {
	console.log("\t" + decimal + "\t:\t" + Numerals.generator.generate(decimal));
};
Numerals.getNumeral = function(decimal) {
	return Numerals.generator.generate(decimal);
};
var haxe = {};
haxe.io = {};
haxe.io.Eof = function() { };
haxe.io.Eof.prototype = {
	toString: function() {
		return "Eof";
	}
};
var oli = {};
oli.num = {};
oli.num.IRomanNumeralGenerator = function() { };
oli.num.RomanNumeralGenerator = function() {
	this.numeralArrays = [[1000,"M"],[900,"CM"],[500,"D"],[400,"CD"],[100,"C"],[90,"XC"],[50,"L"],[40,"XL"],[10,"X"],[9,"IX"],[5,"V"],[4,"IV"],[1,"I"],[0,""]];
};
oli.num.RomanNumeralGenerator.__interfaces__ = [oli.num.IRomanNumeralGenerator];
oli.num.RomanNumeralGenerator.prototype = {
	generate: function(decimal) {
		return this.getNumeral(decimal);
	}
	,getNumeral: function(decimal,numeral) {
		if(numeral == null) numeral = "";
		if(decimal == 0) return numeral;
		var arr = this.getNumeralArray(decimal);
		return this.getNumeral(decimal - arr[0] | 0,numeral + arr[1]);
	}
	,getNumeralArray: function(decimal) {
		var _g = 0;
		var _g1 = this.numeralArrays;
		while(_g < _g1.length) {
			var arr = _g1[_g];
			++_g;
			if(decimal >= arr[0]) return arr;
		}
		var map = [1000,"M"];
		return map;
	}
};
Numerals.main();
})();
