
#pragma warning disable 109, 114, 219, 429, 168, 162
namespace oli.num{
	public  interface IRomanNumeralGenerator : global::haxe.lang.IHxObject {
		   string generate(int @decimal);
		
		   global::Array getNumeralArray(int @decimal);
		
	}
}


