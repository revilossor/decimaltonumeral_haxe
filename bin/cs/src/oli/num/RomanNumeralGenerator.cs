
#pragma warning disable 109, 114, 219, 429, 168, 162
namespace oli.num{
	public  class RomanNumeralGenerator : global::haxe.lang.HxObject, global::oli.num.IRomanNumeralGenerator {
		public    RomanNumeralGenerator(global::haxe.lang.EmptyObject empty){
			unchecked {
				#line 3 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
				{
				}
				
			}
			#line default
		}
		
		
		public    RomanNumeralGenerator(){
			unchecked {
				#line 8 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
				global::oli.num.RomanNumeralGenerator.__hx_ctor_oli_num_RomanNumeralGenerator(this);
			}
			#line default
		}
		
		
		public static   void __hx_ctor_oli_num_RomanNumeralGenerator(global::oli.num.RomanNumeralGenerator __temp_me22){
			unchecked {
				#line 8 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
				{
					#line 5 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
					__temp_me22.numeralArrays = new global::Array<object>(new object[]{new global::Array<object>(new object[]{1000, "M"}), new global::Array<object>(new object[]{900, "CM"}), new global::Array<object>(new object[]{500, "D"}), new global::Array<object>(new object[]{400, "CD"}), new global::Array<object>(new object[]{100, "C"}), new global::Array<object>(new object[]{90, "XC"}), new global::Array<object>(new object[]{50, "L"}), new global::Array<object>(new object[]{40, "XL"}), new global::Array<object>(new object[]{10, "X"}), new global::Array<object>(new object[]{9, "IX"}), new global::Array<object>(new object[]{5, "V"}), new global::Array<object>(new object[]{4, "IV"}), new global::Array<object>(new object[]{1, "I"}), new global::Array<object>(new object[]{0, ""})});
				}
				
			}
			#line default
		}
		
		
		public static  new object __hx_createEmpty(){
			unchecked {
				#line 3 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
				return new global::oli.num.RomanNumeralGenerator(((global::haxe.lang.EmptyObject) (global::haxe.lang.EmptyObject.EMPTY) ));
			}
			#line default
		}
		
		
		public static  new object __hx_create(global::Array arr){
			unchecked {
				#line 3 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
				return new global::oli.num.RomanNumeralGenerator();
			}
			#line default
		}
		
		
		public  global::Array<object> numeralArrays;
		
		public virtual   string generate(int @decimal){
			unchecked {
				#line 10 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
				return this.getNumeral(@decimal, default(string));
			}
			#line default
		}
		
		
		public virtual   string getNumeral(int @decimal, string numeral){
			unchecked {
				#line 12 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
				if (string.Equals(numeral, default(string))) {
					#line 12 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
					numeral = "";
				}
				
				#line 13 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
				if (( @decimal == 0 )) {
					#line 13 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
					return numeral;
				}
				
				#line 14 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
				global::Array arr = this.getNumeralArray(@decimal);
				return this.getNumeral(((int) (( ((double) (@decimal) ) - ((double) (global::haxe.lang.Runtime.toDouble(arr[0])) ) )) ), global::haxe.lang.Runtime.concat(numeral, global::Std.@string(arr[1])));
			}
			#line default
		}
		
		
		public virtual   global::Array getNumeralArray(int @decimal){
			unchecked {
				#line 18 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
				{
					#line 18 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
					int _g = 0;
					#line 18 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
					global::Array<object> _g1 = this.numeralArrays;
					#line 18 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
					while (( _g < _g1.length )){
						#line 18 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
						global::Array arr = ((global::Array) (_g1[_g]) );
						#line 18 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
						 ++ _g;
						if (( global::haxe.lang.Runtime.compare(@decimal, arr[0]) >= 0 )) {
							#line 19 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
							return arr;
						}
						
					}
					
				}
				
				#line 21 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
				global::Array map = new global::Array<object>(new object[]{1000, "M"});
				return map;
			}
			#line default
		}
		
		
		public override   object __hx_setField(string field, int hash, object @value, bool handleProperties){
			unchecked {
				#line 3 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
				switch (hash){
					case 1417216824:
					{
						#line 3 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
						this.numeralArrays = ((global::Array<object>) (global::Array<object>.__hx_cast<object>(((global::Array) (@value) ))) );
						#line 3 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
						return @value;
					}
					
					
					default:
					{
						#line 3 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
						return base.__hx_setField(field, hash, @value, handleProperties);
					}
					
				}
				
			}
			#line default
		}
		
		
		public override   object __hx_getField(string field, int hash, bool throwErrors, bool isCheck, bool handleProperties){
			unchecked {
				#line 3 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
				switch (hash){
					case 52859537:
					{
						#line 3 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
						return ((global::haxe.lang.Function) (new global::haxe.lang.Closure(((object) (this) ), ((string) ("getNumeralArray") ), ((int) (52859537) ))) );
					}
					
					
					case 2065850856:
					{
						#line 3 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
						return ((global::haxe.lang.Function) (new global::haxe.lang.Closure(((object) (this) ), ((string) ("getNumeral") ), ((int) (2065850856) ))) );
					}
					
					
					case 1503813429:
					{
						#line 3 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
						return ((global::haxe.lang.Function) (new global::haxe.lang.Closure(((object) (this) ), ((string) ("generate") ), ((int) (1503813429) ))) );
					}
					
					
					case 1417216824:
					{
						#line 3 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
						return this.numeralArrays;
					}
					
					
					default:
					{
						#line 3 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
						return base.__hx_getField(field, hash, throwErrors, isCheck, handleProperties);
					}
					
				}
				
			}
			#line default
		}
		
		
		public override   object __hx_invokeField(string field, int hash, global::Array dynargs){
			unchecked {
				#line 3 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
				switch (hash){
					case 52859537:
					{
						#line 3 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
						return this.getNumeralArray(((int) (global::haxe.lang.Runtime.toInt(dynargs[0])) ));
					}
					
					
					case 2065850856:
					{
						#line 3 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
						return this.getNumeral(((int) (global::haxe.lang.Runtime.toInt(dynargs[0])) ), global::haxe.lang.Runtime.toString(dynargs[1]));
					}
					
					
					case 1503813429:
					{
						#line 3 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
						return this.generate(((int) (global::haxe.lang.Runtime.toInt(dynargs[0])) ));
					}
					
					
					default:
					{
						#line 3 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
						return base.__hx_invokeField(field, hash, dynargs);
					}
					
				}
				
			}
			#line default
		}
		
		
		public override   void __hx_getFields(global::Array<object> baseArr){
			unchecked {
				#line 3 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
				baseArr.push("numeralArrays");
				#line 3 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
				{
					#line 3 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\oli\\num\\RomanNumeralGenerator.hx"
					base.__hx_getFields(baseArr);
				}
				
			}
			#line default
		}
		
		
	}
}


