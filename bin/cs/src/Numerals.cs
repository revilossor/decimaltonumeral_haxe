
#pragma warning disable 109, 114, 219, 429, 168, 162
public  class Numerals : global::haxe.lang.HxObject {
	public static void Main(){
		global::cs.Boot.init();
		main();
	}
	public    Numerals(global::haxe.lang.EmptyObject empty){
		unchecked {
			#line 4 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\Numerals.hx"
			{
			}
			
		}
		#line default
	}
	
	
	public    Numerals(){
		unchecked {
			#line 4 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\Numerals.hx"
			global::Numerals.__hx_ctor__Numerals(this);
		}
		#line default
	}
	
	
	public static   void __hx_ctor__Numerals(global::Numerals __temp_me6){
		unchecked {
			#line 4 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\Numerals.hx"
			{
			}
			
		}
		#line default
	}
	
	
	public static  global::oli.num.RomanNumeralGenerator generator;
	
	public static   void main(){
		unchecked {
			#line 8 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\Numerals.hx"
			global::Numerals.generator = new global::oli.num.RomanNumeralGenerator();
			#line 10 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\Numerals.hx"
			{
				#line 10 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\Numerals.hx"
				int _g = 0;
				#line 10 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\Numerals.hx"
				global::Array<object> _g1 = global::Sys.args();
				#line 10 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\Numerals.hx"
				while (( _g < _g1.length )){
					#line 10 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\Numerals.hx"
					string arg = global::haxe.lang.Runtime.toString(_g1[_g]);
					#line 10 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\Numerals.hx"
					 ++ _g;
					{
						#line 11 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\Numerals.hx"
						int @decimal = global::Std.parseInt(arg).@value;
						#line 11 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\Numerals.hx"
						global::haxe.Log.trace.__hx_invoke2_o(default(double), global::haxe.lang.Runtime.concat(global::haxe.lang.Runtime.concat(global::haxe.lang.Runtime.concat("\t", global::haxe.lang.Runtime.toString(@decimal)), "\t:\t"), global::Numerals.generator.generate(@decimal)), default(double), new global::haxe.lang.DynamicObject(new global::Array<int>(new int[]{302979532, 1547539107, 1648581351}), new global::Array<object>(new object[]{"printNumeral", "Numerals", "Numerals.hx"}), new global::Array<int>(new int[]{1981972957}), new global::Array<double>(new double[]{((double) (19) )})));
					}
					
				}
				
			}
			
		}
		#line default
	}
	
	
	public static   void printNumeral(int @decimal){
		unchecked {
			#line 19 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\Numerals.hx"
			global::haxe.Log.trace.__hx_invoke2_o(default(double), global::haxe.lang.Runtime.concat(global::haxe.lang.Runtime.concat(global::haxe.lang.Runtime.concat("\t", global::haxe.lang.Runtime.toString(@decimal)), "\t:\t"), global::Numerals.generator.generate(@decimal)), default(double), new global::haxe.lang.DynamicObject(new global::Array<int>(new int[]{302979532, 1547539107, 1648581351}), new global::Array<object>(new object[]{"printNumeral", "Numerals", "Numerals.hx"}), new global::Array<int>(new int[]{1981972957}), new global::Array<double>(new double[]{((double) (19) )})));
		}
		#line default
	}
	
	
	public static   string getNumeral(int @decimal){
		unchecked {
			#line 22 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\Numerals.hx"
			return global::Numerals.generator.generate(@decimal);
		}
		#line default
	}
	
	
	public static  new object __hx_createEmpty(){
		unchecked {
			#line 4 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\Numerals.hx"
			return new global::Numerals(((global::haxe.lang.EmptyObject) (global::haxe.lang.EmptyObject.EMPTY) ));
		}
		#line default
	}
	
	
	public static  new object __hx_create(global::Array arr){
		unchecked {
			#line 4 "c:\\Users\\Admin\\dev\\haxe\\numerals\\src\\Numerals.hx"
			return new global::Numerals();
		}
		#line default
	}
	
	
}


