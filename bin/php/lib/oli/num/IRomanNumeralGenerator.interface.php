<?php

interface oli_num_IRomanNumeralGenerator {
	//;
	function generate($decimal);
	function getNumeralArray($decimal);
}
