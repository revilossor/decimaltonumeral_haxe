<?php

class oli_num_RomanNumeralGenerator implements oli_num_IRomanNumeralGenerator{
	public function __construct() {
		if(!php_Boot::$skip_constructor) {
		$this->numeralArrays = (new _hx_array(array((new _hx_array(array(1000, "M"))), (new _hx_array(array(900, "CM"))), (new _hx_array(array(500, "D"))), (new _hx_array(array(400, "CD"))), (new _hx_array(array(100, "C"))), (new _hx_array(array(90, "XC"))), (new _hx_array(array(50, "L"))), (new _hx_array(array(40, "XL"))), (new _hx_array(array(10, "X"))), (new _hx_array(array(9, "IX"))), (new _hx_array(array(5, "V"))), (new _hx_array(array(4, "IV"))), (new _hx_array(array(1, "I"))), (new _hx_array(array(0, ""))))));
	}}
	public $numeralArrays;
	public function generate($decimal) {
		return $this->getNumeral($decimal, null);
	}
	public function getNumeral($decimal, $numeral = null) {
		if($numeral === null) {
			$numeral = "";
		}
		if($decimal === 0) {
			return $numeral;
		}
		$arr = $this->getNumeralArray($decimal);
		return $this->getNumeral(Std::int($decimal - $arr[0]), _hx_string_or_null($numeral) . _hx_string_or_null($arr[1]));
	}
	public function getNumeralArray($decimal) {
		{
			$_g = 0;
			$_g1 = $this->numeralArrays;
			while($_g < $_g1->length) {
				$arr = $_g1[$_g];
				++$_g;
				if($decimal >= $arr[0]) {
					return $arr;
				}
				unset($arr);
			}
		}
		$map = (new _hx_array(array(1000, "M")));
		return $map;
	}
	public function __call($m, $a) {
		if(isset($this->$m) && is_callable($this->$m))
			return call_user_func_array($this->$m, $a);
		else if(isset($this->__dynamics[$m]) && is_callable($this->__dynamics[$m]))
			return call_user_func_array($this->__dynamics[$m], $a);
		else if('toString' == $m)
			return $this->__toString();
		else
			throw new HException('Unable to call <'.$m.'>');
	}
	function __toString() { return 'oli.num.RomanNumeralGenerator'; }
}
